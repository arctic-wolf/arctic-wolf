The `ci` command allows the developer to run continuous integration on the project. The continuous integration is configurable through this command. If the build script is run without a command it also run the continuous integration with the configured settings.

Usage:
    build.sh ci [options] [PROJECT_PATH]

Flags:

Options:

Variables:
    PROJECT_PATH    Project root path, or some path in the project root
