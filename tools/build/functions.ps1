#
# build/add_command.sh - UNIX build script (add command)
#

#
# This script contains the functions shared by several of the commands and/or the main
# build script.
#
#---
# PACKAGE functions
#
# * Check-Package   Checks if a package with the name given exists and whether it's in
#                   the given path.
#
#---
# MISC functions
#
# * display_help    Display the help information on the selected command or general
#                   information is no command is given.
#
#---
# CARGO functions
#
# * cargo_add_to_workspace  Add a package to the workspace members.
#
#---
# GITLAB functions
#
# * gitlab_check_project    Check if a project exists and whether it's in the given
#                           subgroup.
# * gitlab_create_project   Create a public project on Gitlab.
#

#----------------------------------------------------------------------------------------
# PACKAGE functions
#------------------

#---
# Check Package
#
# Usage:
#   Check-Package <PATH> <PACKAGE>
#
# Returns:
#   0   Package doesn't exist
#   1   An error occured calling the function
#   2   Package exists in the path given
#   3   Package exists in another path
#---
function Check-Package
{
    param (
        [Parameter(Mandatory)]
        [string]$path,

        [Parameter(Mandatory)]
        [string]$package
    )

    $package_path = "$path/$package/Cargo.toml"

    if (Test-Path $package_path) {
        return 2
    } else {
        if (-Not (Test-Path -Path $global.script_path -Filter "$package/Cargo.toml")) {
            return 0
        }
    }

    return 3
}

#----------------------------------------------------------------------------------------
# MISC functions
#---------------

#---
# Display help
#
# Usage:
#   Display-Help
#
# Exits with success status
#---
function Display-Help
{
    $info_file = "$tools_build_path/docs/general.txt"
    
    switch ($cmd) {
        $NO_COMMAND {
            if ($ci) {
                Write-Host ("build.sh (continuous integration)")
                $info_file = "$tools_build_path/docs/ci.txt"
            } else {
                Write-Host ("build.sh (general)")
                $info_file = "$tools_build_path/docs/general.txt"
            }
        }
        $CMD_ADD {
            Write-Host ("build.sh (add command)")
            $info_file = "$tools_build_path/docs/add_command.txt"
        }
        $CMD_BUILD {
            Write-Host ("build.sh (build command)")
            $info_file = "$tools_build_path/docs/build_command.txt"
        }
        $CMD_RUN {
            Write-Host ("build.sh (run command)")
            $info_file = "$tools_build_path/docs/run_command.txt"
        }
        $CMD_TEST {
            Write-Host ("build.sh (test command)")
            $info_file = "$tools_build_path/docs/test_command.txt"
        }
    }
   
    Write-Host ("")
    Get-Content $info_file
    Write-Host ("")
    
    Exit 0
}

#----------------------------------------------------------------------------------------
# CARGO functions
#----------------

#---
# Add a member to a Cargo workspace
#
# Usage:
#   Cargo-Add-To-Workspace <PATH> <PACKAGE>
#
# Returns:
#   0   The project is added
#   1   An error occured adding the member
#---
function Cargo-Add-To-Workspace
{
    param (
        [Parameter(Mandatory)]
        [string]$new_path,

        [Parameter(Mandatory)]
        [string]$new_package
    )

    $new_member = "$new_path/$new_package"

    $package = "tundra-wolf"

    $tmp_cargo = "$tmp_path/Cargo.toml"
    $tmp_cargo_packages = "$tmp_path/Cargo.packages"

    if (Test-Path $tmp_cargo -PathType Leaf) {
        Remove-Item -Path $tmp_cargo
    }
    if (Test-Path $tmp_cargo_packages -PathType Leaf) {
        Remove-Item -Path $tmp_cargo_packages
    }

    ForEach ($package in $packages.keys) {
        "$($packages.$package)" >> $tmp_cargo_packages
    }
    "$new_member" >> $tmp_cargo_packages

    Get-Content -Path $tmp_cargo_packages | Sort-Object > $tmp_cargo_packages.sorted

    "[workspace]" > $tmp_cargo
    "members = [" >> $tmp_cargo

    ForEach ($member in Get-Content "$tmp_cargo_packages.sorted") {
        "    `"$member`"," >> $tmp_cargo
    }

    "]" >> $tmp_cargo
    "resolver = `"2`"" >> $tmp_cargo
    "" >> $tmp_cargo

    Copy-Item $tmp_cargo -Destination "$script_path/Cargo.toml"

    Remove-Item -Path $tmp_cargo_packages
    Remove-Item -Path "$tmp_cargo_packages.sorted"
    Remove-Item -Path $tmp_cargo

    return 0
}

#----------------------------------------------------------------------------------------
# GITLAB functions
#-----------------

#---
# Check project on Gitlab
#
# Usage:
#   Gitlab-Check-Project <SUBGROUOP> <PROJECT>
#
# Returns:
#   0   Project doesn't exist
#   1   An error occured calling the function
#   2   Project exists in the subgroup given
#   3   Project exists in another subgroup
#---
function Gitlab-Check-Project
{
    param (
        [Parameter(Mandatory)]
        [string]$subgroup,

        [Parameter(Mandatory)]
        [string]$project
    )

    $subgroup_package = "$subgroup/$project"

    return 1
}

#---
# Create a new private project on Gitlab
#
# Usage:
#   gitlab_create_project <SUBGROUP> <PROJECT>
#
# Returns:
#   0   Project created successfully
#   1   An error occured creating the project
#---
function Gitlab-Create-Project
{
    param (
        [Parameter(Mandatory)]
        [string]$subgroup,

        [Parameter(Mandatory)]
        [string]$project
    )

    # Create remote project
    git remote add -t main origin "git@gitlab.com:$subgroup/$project.git"
    if ($LASTEXITCODE -ne 0) {
        return 1
    }

    # Push changes to the remote repository
    git push --set-upstream origin master
    if ($LASTEXITCODE -ne 0) {
        return 1
    }

    Write-Host ("[Warning]")
    Write-Host ("    Please make sure the project is publicly accessible on Gitlab? Currently")
    Write-Host ("    we have no way to do this programmatically.")
    Write-Host ("[Warning]")

    return 0
}
